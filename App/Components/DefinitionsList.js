var React = require('react-native');
var api = require('../Utils/api');
var Separator = require('./Helpers/Separator');

var {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  ActivityIndicatorIOS
} = React;

var styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'column',
    flex: 1,
    padding: 10
  }
});

class DefinitionsList extends React.Component{
  render() {
    var definitions = this.props.definitions;

    var list = definitions.map((item, index) => {
       return (
         <View key={index}>
           <View style={styles.rowContainer}>
             <Text>{definitions[index].name}</Text>
           </View>
           <Separator />
         </View>
       )
     });

    return(
      <View style={styles.mainContainer}>
        {list}
      </View>
      )
  }
};

module.exports = DefinitionsList;
